SMACK I: history of the POSIX operating systems and basic tools
---------------------------------------------------------------

The session plan is
[available on GitLab](https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/smack-1-posix.md),
and has also been used in a talk that has been [recorded on YouTube](https://www.youtube.com/watch?v=OR3tWKKmhEM).

### Introduction (5 min, Carlos)
1. CUSI
2. SMACK and IACTalks
3. Upcoming SMACKs (list in `README.md`).


### Quick History (15 min, Mohammad)
Slides: http://akhlaghi.org/pdf/posix-family.pdf
1. UNIX
2. BSD
3. GNU
4. Linux.
5. POSIX (IEEE standard that define Unix-like operating systems).
6. Licenses (GPL, MIT, BSD and etc) and their parallel history.


### Command line (30 min, Carlos)
1. The shell: `csh`, `bash`, `echo`.
2. Space-time: `whoami`, `date`, `pwd`.
3. Help: `which`, `apropos`, `man`, `info`, `history`
4. Directories  `/`, `/home`, `/bin`, `mkdir`, `rmdir`, `cd`.
5. Files: `ls`, `ls -l`, `cat`, `rm`, `editors` (`nano`, `vim`, `emacs`).
6. Wildcards: `*`, `?`.
7. I/O:  standard I/O, `>`, `<`, `cat`, `paste`, `seq`.
8. Streaming: `|`, `sed`, `awk` (in passing, to be developed in SMACK-2).
9. Other utilities: `sort`, `grep` (in passing, more details in SMACK-2).
10. Computer resources (drives: `df`, `du`), memory (`top`, `htop`), processors (`/proc/cpuinfo`).


### The IAC network (15 min, Carlos)
1. SSH
2. VPN
3. Cross-mounting drives
4. Burros: `denso`, `dejar`, etc.
5. SuperComputing: `deimos`, LaPalma, TeideHPC, RES...
