﻿SMACK 14: Make (A powerful tool to manage your projects)
--------------------
Abstract:

Did you ever want to re-run your project from the beginning, but run
into trouble because you forgot one step? Do you want to run just one
part of your project and ignore the rest? Do you want to run it in
parallel with many different inputs efficiently and optimally use all
the cores of your computer? Do you want to design a modular project,
with re-usable parts, avoiding long files that are hard to debug?

Make is the well-tested solution to all these problems. Make is
independent of the programming language you use. Instead of having a
long code that is hard to debug, you can connect different small parts
as a chain. Make will allow you to automate your project and control
its different parts, or run independent parts in parallel. This SMACK
seminar will give an overview of this powerful tool.

- **Recorded video:** https://www.youtube.com/watch?v=65EhAL08Mb8





### Steps that we are goign to follow in this Smack:

0-Downloading and installing the GNUmake from the source:

Gnumake website: https://www.gnu.org/software/make/

```shell
	# Get the file from the main GNU ftp server(the latest version)
	wget https://ftp.gnu.org/gnu/make/make-4.3.tar.gz

	# Unpack the source file
	tar -xf make-4.3.tar.gz

	# Configure and make it
	./configure
	make
	sudo make install
```

1- Lets start with a simple scenario, writting a bash script and then we
will try to write it as makefile in next steps:

### Working senario:

We want to download a Gaia catalog, limite the downloaded catalog
and then crop the objects in the limited catalog on some images.

```shell

# Define the build directory
bdir=./build-shell
if ! [ -d $bdir ]; then mkdir $bdir; fi

# Downloading gaia catalog
gaia=$bdir/gaia.fits
if ! [ -f $gaia ]; then
    astquery gaia --dataset=edr3 \
	     --center=23.462042,30.660222 --radius=1/60 \
             -csource_id,ra,dec,phot_g_mean_mag \
	     --range=phot_g_mean_mag,7:20 -o$gaia
fi


# Crop the stars based on the limited catalog over the images
list=$(ls ./inputdir/*.fits)
img=$(echo $list | sed -e's|./inputdir/| |g' -e's/.fits/ /g')

for i in $img; do
    cropdir=$bdir/$i
    if ! [ -d $cropdir ];then mkdir $cropdir; fi
    target=$cropdir/done.txt
    if ! [ -f $target ]; then
	astcrop  ./inputdir/$i.fits --catalog=$gaia --mode=wcs \
		 --width=0.1  --coordcol=ra --coordcol=dec \
		 --namecol=source_id --output=$cropdir
	echo "Done" > $target
    fi
done


```

2- Make rules and target for a makefile (The most IMPORTANT part)

Defination of target(make variable) >>>>>> target
target=.....
target : prerequisites
       commands to build target(OR TARGETS)

3-Lets start with senario which you want to download a Gaia datasets
throught the command line:
```shell
	gaiacatalog=./gaia.fits
	$(gaiacatalog):
		astquery gaia --dataset=edr3 \
		--center=23.462042,30.660222 --radius=1/60\
		-csource_id,ra,dec --range=phot_g_mean_mag,7:20 \
		--output=$@

	# Now imagine that you want to add a line to your command(code).

	gaiacatalog=./gaia.fits
	$(gaiacatalog):
		astquery gaia --dataset=edr3 \
		--center=113.8729761,31.9027152 --radisu=3/60\
		-csource_id,ra,dec,phot_g_mean_mag \
		--range=phot_g_mean_mag,7:20 --output=$@
```

	# IT DOESN'T WORK(!!!!!!), code has been updated but the
          target no, what we can do:


4- Changing the recepi(command) and force the make to update the target

```shell
        include configuration-gaia.txt

	gaiacatalog=./gaia.fits
	$(gaiacatalog): configuration-gaia.txt
	        echo "check ra dec radius: $(ra) $(dec) $(radius)"
		astquery gaia --dataset=edr3 \
		--center=$(ra),$(dec) --radisu=$(radius)\
		-csource_id,ra,dec,phot_g_mean_mag \
		--range=phot_g_mean_mag,$(min-mag):$(max-mag) \
		--output=$@

	# Still it doesn't work, the problem is that make will run
          each line in different shells, to solve this problem, we
          have to specify that run the make an one shell:

	# Run all the commands in the recipe in one shell
	.ONESHELL:

	# Import using include
	include configuration-gaia.txt

	gaiacatalog=./gaia.fits
	$(gaiacatalog): configuration-gaia.txt

		astquery gaia --dataset=edr3 \
		--center=$(ra),$(dec) --radisu=$(radius)\
		-csource_id,ra,dec,phot_g_mean_mag \
		--range=phot_g_mean_mag,$(min-mag):$(max-mag) \
		--output=$@
```

5-Run the exact simple makefile but in case that your command is python code:
(Make independent of the programming language you use)

```shell

	# Run all the commands in the recipe in one shell
	.ONESHELL:

	# Import using include
	include configuration-gaia.txt

	gaiacatalog=./gaia.fits
	$(gaiacatalog): configuration-gaia.txt download-gaia2-catalogue.py

        python3.9 download-gaia2-catalogue.py $(ra) $(dec) \
                  $(dra) $(ddec) $@
```

6-Lets try to write directly python command in makefile!!:

```shell

	SHELL = /usr/local/bin/python3.9

	.ONESHELL:

	# Import using include
	include configuration-gaia.txt

	gaiacatalog = ./gaia.fits
	$(gaiacatalog): configuration-gaia.txt

        		# Import modules
        		import sys
        		from astroquery.gaia import Gaia

			# Define the input parameters
        		tablename_all = 'gaiadr2.gaia_source'
        		t = Gaia.load_table(tablename_all)
        		ra = $(ra)
        		dec = $(dec)
        		d_ra = $(dra)
        		d_dec = $(ddec)

		        # Define the searching area of sources
		        ra_min = str(ra - d_ra)
		        ra_max = str(ra + d_ra)
		        dec_min = str(dec - d_dec)
		        dec_max = str(dec + d_dec)



		        # SQL string type
		        query_all = (' select                  '  +
		        '        ra, dec,         '  +
		        '        phot_g_mean_mag  '  +
		        ' from  ' + tablename_all    +
		        ' where '                    +
		        '        ra  > ' + ra_min    +
		        ' and    ra  < ' + ra_max    +
		        ' and    dec > ' + dec_min   +
		        ' and    dec < ' + dec_max )


		        # Lunch the SQL petition and get the data
		        job = Gaia.launch_job_async(query_all)
		        table = job.get_data()


		        # Save the data as .fits table
		        table.write("$@", format='fits', overwrite=True)

```

7- How we can have a control on the direcotry of the output.

```shell
	# Make settings
	# Run all the commands in the recipe in one shell
	.ONESHELL:


	indir=/Users/scratchpad/smack-test-dir/inputdir
	list=$(shell ls $(indir)/*.fits)
	bdir=/Users/scratchpad/smack-test-dir/build-dir
	$(bdir):; mkdir $@

	# Import using include
	include configuration-gaia.txt

	# Downloading the gaia catalog:
	$(gaia):configuration-gaia.txt
        astquery gaia --dataset=edr3 \
                 --center=$(ra),$(dec) --radisu=$(radius)\
                 -csource_id,ra,dec,phot_g_mean_mag \
		 --range=phot_g_mean_mag,$(min-mag):$(max-mag) \
		 --output=$@


	# As you can see the only target which has been made above was
        #  the $(bdir). Why the make doesn't consider the $(gaia)
        #  targe?  To solve this problem, in case which you have more
        #  than one target, you need to define the final target of the
        #  makefile.


	# Make settings

	# Run all the commands in the recipe in one shell
	.ONESHELL:

	# Defination of final target
	all: final


	indir=/Users/scratchpad/smack-test-dir/inputdir
	list=$(shell ls $(indir)/*.fits)
	bdir=/Users/scratchpad/smack-test-dir/build-dir
	$(bdir):; mkdir $@

	# Import using include
	include configuration-gaia.txt

	# Downloading the gaia catalog:
	$(gaia):configuration-gaia.txt
        astquery gaia --dataset=edr3 \
                 --center=$(ra),$(dec) --radius=$(radius)\
                 -csource_id,ra,dec,phot_g_mean_mag \
		 --range=phot_g_mean_mag,$(min-mag):$(max-mag) \
		 --output=$@

	# Final target
	final: $(gaia)

```

8-Parallelization and modularity in make: How we can have different
targets in make, make all of them or ignore one of them

```shell

	# Final target
	all: final

	# Make settings
	# Make all the commands in the recipe in one shell
	.ONESHELL:

	# Making the build directory.
	bdir=/scratch/tmp/make/smack/build-dir
	$(bdir):; mkdir $@

	# Import using include
	include configuration-gaia.txt

	# Downloading the gaia catalog.
	gaia=$(bdir)/gaia.fits
	$(gaia): configuration-gaia.txt | $(bdir)
		 astquery gaia --dataset=edr3  --center=$(ra),$(dec) \
		 --radius=$(radius) -csource_id,ra,dec,phot_g_mean_mag \
		 --range=phot_g_mean_mag,$(min-mag):$(max-mag) \
		 --output=$@


        # Crop these stars from 5 exposures.
	indir=/scratch/tmp/make/smack/inputdir
	list=$(notdir $(subst .fits,,$(wildcard $(indir)/*.fits)))
	cropimg=$(foreach d,$(list),$(bdir)/$(d)/done.txt)
	cropdir=$(subst /done.txt,,$(cropimg))
	$(cropdir): | $(bdir); mkdir $@
	$(cropimg): $(bdir)/%/done.txt: $(catlim) $(indir)/%.fits | $(bdir)/%
		    astcrop $(indir)/$*.fits --catalog=$(gaia) \
	            	    --mode=wcs --width=0.1  --coordcol=ra \
			    --coordcol=dec --namecol=source_id -o$(bdir)/$*
	touch $@


	# Final target
	final: $(cropimg)

```
