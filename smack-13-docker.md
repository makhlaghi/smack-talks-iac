SMACK 13: Containers
--------------------

SMACK 13 is focused on
[Containers](https://en.wikipedia.org/wiki/OS-level_virtualization). It
starts with an introduction to the concept (in slides), followed by a
hands-on demonstration of Docker and Apptainer (previously known as
Singularity).

## Recorded video of SMACK 13:

This SMACK was presented on December 9th, 2021, as [part of IACTalks](http://iactalks.iac.es/talks/view/1530) and a recording of this session is available on YouTube: https://www.youtube.com/watch?v=e6-XBbbAsA8

## Introduction and Apptainer lecture notes

The introduction and lecture notes to demonstrate Apptainer (previously known as Singularity) are available in these slides: https://gitlab.com/makhlaghi/smack-talks-iac/-/raw/master/Slides/smack-13-containers.pdf

## Docker lecture notes

[Docker containers](https://www.docker.com/resources/what-container)
are a common way to build projects in an independent _filesystem_, and
an almost independent _operating system_. Its containers therefore
allow using GNU/Linux operating systems within proprietary operating
systems like macOS or Windows, but without the overhead and huge file
size of virtual machines.

If you are already using a GNU/Linux operating system, containers
(like those offered by Docker) allow you to work on separate projects
within an independent filesystem (thus not making any changes in your
operating system). Furthermore containers allow easy movement of a
built environment from one system to another without rebuilding.

However, Docker images also have certain caveats:

 - They need root permissions on the host operating system. Therefore
   they are hard/impossible to run on many large HPCs (which don't
   allow root previlages).
 - Docker images are large files (+1 Gigabytes).
 - Docker images are binary files (only readable by Docker
   itself!). So they may not be usable in the future (for example with
   new Docker versions not reading old images). Containers are thus
   good for temporary/testing phases of a project, but shouldn't be
   what you archive for the long term!

Therefore, if you want to save and move your projects with a Docker
image, be sure to commit all your project's source files and push them
to your external Git repository (you can do these within the Docker
image as explained below). This way, you can always recreate the
container with future technologies too.

Generally, if you are developing a project within a container, its
good practice to recreate it from scratch every once in a while, to
make sure you haven't forgot to include parts of your work in your
project's version-controlled source (remember that the Docker image
itself is not good for archival). In the sections below we also
describe how you can use the container only for the software
environment and keep your data and project source on your host.

## Setup your first Docker image and container

If you are on a GNU/Linux operating system, the best way to install
Docker is through your package manager (for example `sudo apt install
docker` on Debian-based operating systems, like Ubuntu). Docker has a
lot of dependencies and installing it from source will be very
hard. For [macOS](https://docs.docker.com/desktop/mac/install) or
[Windows 10](https://docs.docker.com/desktop/windows/install/), you
can use its own installation file.

You should remember that Docker only runs as root! So before any
futher operations, you need to become root:

```shell
$ sudo su
```

 0. [Only for **GNU/Linux** operating systems] After you install
    Docker, on GNU/Linux systems, to start using it, you need to
    activate its
    [Daemon](https://en.wikipedia.org/wiki/Daemon_(computing)) (a
    program that runs on the background to keep a connection between
    your containers on different terminals):

    ```shell
    systemctl start docker
    ```

 1. To create a new Docker _image_, you need a `Dockerfile`. This file
    contains instructions on building and setting up the operating
    system within it. Let's start with a minimal Debian operating
    system. Open a plain-text editor in an _empty directory_ and put
    the line `FROM debian:stable-slim` inside a file called
    `Dockerfile` (the contents of the directory will go into your
    "build context", so its best to build your image from a
    `Dockerfile` in an empty directory):

    ```shell
    mkdir my-docker
    cd my-docker
    emacs Dockerfile
    cat Dockerfile
    FROM debian:stable-slim
    ```

    Within the `Dockerfile`, all Docker commands start in full-caps
    (`FROM` in the line above). You can choose from [versions of
    Debian](https://hub.docker.com/_/debian/), or use different
    [versions of Ubuntu](https://hub.docker.com/_/ubuntu), or even a
    very minimal [Alpine GNU/Linux](https://hub.docker.com/_/alpine).

 2. To create your first Docker image, you can simply run Docker on
    the `Dockerfile` with the command below. By default, Docker will
    look for this file, so as long as its in the current directory,
    you don't even need to give its name:

    ```shell
    docker build ./
    ```

    This will download the "slim" (~30Mb) Debian-stable operating
    system image into the library of images in Docker of your
    operating system. With the command below, you can always see the
    list of _image_s on your operating system (if this is the first
    time you are using Docker, the `stable-slim` image should be the
    only item in the list):

    ```shell
    docker image list
    ```

    If you had used the Apline GNU/Linux (which is designed to be the
    bare minimum of any utility in the OS), the download size would
    have been about 5Mb!

 3. Let's go inside the new operating system and have a look. When you
    "run" a Docker image, a _container_ is created and executed. So
    **Docker containers are instances of an archived Docker
    image**. The `-i` in the command below stands for "interactive",
    and the `-t' stands for "tag" (or name of the image).

    ```shell
    docker run -i -t debian:stable-slim
    ```

    You will notice that the prompt of your command-line changed! To
    something like `root@XXXXX:/` (where the `XXXXX` part is a
    seemingly random series of numbers and alphabetic characters, and
    is called a hash, it identifies the "container" you are running).

    You can always leave your interactive container and come back to
    your operating system with the `exit` command:

    ```shell
    exit
    ```

    Now, re-run the `docker run` command above (to enter a new
    "container" created from the `debian:stable-slim` "image"), you
    will see that the hash has changed: you are now running a
    different instance of the image, or a different "container"!

 4. To see that you are in a separate operating system, run the
    command below while you are in your container (to first see, then
    count the number of programs available in the operating system's
    `/usr/bin`, that contains the core programs like `ls` or `cp`):

    ```shell
    docker run -i -t debian:stable-slim
    ls /usr/bin
    ls /usr/bin | wc -l
    ```

    Now exit the container and repeat the commands:

    ```shell
    exit
    ls /usr/bin | wc -l
    ```

    Do you see how the contents of the same core directory differ so
    significantly from your host operating system and the Docker
    container?

 5. If you don't want to keep your Docker images any more, you can run
    the command below to delete or remove them. But first, let's list
    the images on your system first, then delete all existing
    containers with the second command (and finally list the
    containers again):

    ```shell
    docker image list
    docker ps -a -q | xargs docker rm
    docker images -a -q | xargs docker rmi -f
    docker image list
    ```

 6. The default image names are usually not too useful! When building
    your images from the `Dockerfile`, you can also give them a "tag"
    (or name) by adding a `-t NAME` option to the `docker build`
    command that we ran before. Let's call it `my-debian`

    ```shell
    docker build -t my-debian ./
    ```

    If you now list the Docker images (command and example output
    below), you will see that `debian:stable-slim` still exists (where
    the `:` separates the `REPOSITORY` column from the `TAG`
    column). However, you now also have a second image that is called
    `my-debian:latest`. The important point is that they both have the
    same `IMAGE ID` (or hash). This is because at the current moment,
    the latter is just a pointer to the first!

    ```shell
    docker image list
    REPOSITORY   TAG           IMAGE ID       CREATED      SIZE
    debian       stable-slim   d35b6367cf8f   8 days ago   80.4MB
    my-debian    latest        d35b6367cf8f   8 days ago   80.4MB
    ```

 7. Now that your image has a name, running it is much more easier:

    ```shell
    docker run -it my-debian
    ```

    We are now in a "container" created from your `my-debian`
    image. Let's make a temporary file, make sure it exists, and check
    where we are, then exit the container:

    ```shell
    echo "My first file in a container!" > first.txt
    ls
    pwd
    exit
    ```

    Now that we are outside the container, you will notice that the
    file doesn't exist and you are in the same `my-docker` directory
    in your operating system (that only has a `Dockerfile` inside of
    it):

    ```shell
    ls
    pwd
    ```

    Let's make a new container from our `my-debian` image, and see if
    the `first.txt` file (that we created before), still exists or
    not?

    ```shell
    docker run -it my-debian
    ls
    ```

    You see that no `first.txt` exists!!! This is a very important
    point in "container"s: they are temporary! This is a very useful
    feature in many scenarios! Where you simply want to test something
    and don't want to preserve the outputs, or worry about any change
    that you make.

 8. In some situations, you want to preserve the files in your
    containers. To demonstrate, let's make a second file in the
    container (the prompt is shown here intentially, because we need
    the hash in the next step):

    ```shell
    # In terminal 1
    root@f5995e56e4ec:/# echo "My second file in a container!" > second.txt
    root@f5995e56e4ec:/# ls
    ```

    When you want to preserve the files in the container, before
    closing (or `exit`ing the container), you need to "commit" the
    container into a new image (for loading later as a new container)!
    To commit a "live" container (that is running somewhere on the
    system), you should go into another terminal and use the command
    below which includes the container's hash as well as a name for
    the new

    ```shell
    # In terminal 2 (another terminal) ...
    docker commit f5995e56e4ec with-second
    ```

    Let's now have a look at the container list: you will see that the
    previous `my-debian` container still exists (and still has the
    same hash as it originally had). But the new `with-second` image
    has also been added which has the same size, but with a different
    hash:

    ```shell
    # In terminal 2 (another terminal) ...
    docker image list
    REPOSITORY   TAG           IMAGE ID       CREATED         SIZE
    with-second  latest        c63f8122b9ef   3 seconds ago   80.4MB
    debian       stable-slim   d35b6367cf8f   8 days ago      80.4MB
    my-debian    latest        d35b6367cf8f   8 days ago      80.4MB
    ```

    The differing hash clearly shows that `with-second` is different
    from `my-debian`! But its size is the same, because its only
    difference with `my-debian` is the very small `second.txt` file
    that we created. Infact, Docker will not copy the previous image
    into the new one! It will only preserve the differences! So in the
    case above, the space the three images take on your operating
    system is just 80.4MB.

    Now that you have committed all the changes in your Docker
    container as a Docker image, you can exit the container like
    below. However, you can also still continue working! But any
    change that you make will be lost when you leave, unless you
    commit it again.

    ```shell
    # In terminal 1
    root@f5995e56e4ec:/# exit
    ```

    When selecting a name for the committed image, you can select the
    same name as the original image that the container was created
    from! In this case, the old image will be over-written.

 9. More advanced demonstration and explanation on usage of
    `Dockerfiles` can be see in [the README file of
    Maneage](https://gitlab.com/maneage/project#building-in-docker-containers). Maneage
    (http://maneage.org, short for Managing data lineage) is a
    pipeline system to reproducibly preserve the pipeline of your
    science project in the long term (with the ability to build within
    containers for short-term moving of the project from one computer
    to the next). A separate SMACK will be presented on Maneage in the
    near future.
