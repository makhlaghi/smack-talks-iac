SMACK 15: Programming parallel workloads with PyCOMPSs in La Palma
------------------------------------------------------------------

### Abstract


PyCOMPSs is a task-based programming in Python that enables simple sequential
codes to be executed in parallel in distributed computing platforms.
It is based on the addition of python decorators in the functions of the code
to indicate those that can be executed in parallel between them.
The runtime takes care of the different parallelization and resource management
actions, as well as of the ditribution of the data in the different nodes of
the computing infrastructure.
It is installed in multiple supercomputers of the RES, like MareNostrum 4 and
now LaPalma. The talk will present an overview of PyCOMPSs, two demos with
simple examples and a hands-on in LaPalma on how we can parallelize EMCEE
workloads.

Relevant links:
* Lecture notes (pdf): https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/Slides/smack-15-Programming-with-PyCOMPSs_v0.2.pdf
* Hands-on material: https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/Hands-on/smack-15/
