ADASS 2021: Gnuastro hands-on tutorial for astronomical data analysis
---------------------------------------------------------------------

### Abstract

[Gnuastro](https://www.gnu.org/s/gnuastro)
is an official GNU package of a large [collection of
programs](https://www.gnu.org/software/gnuastro/manual/html_node/Gnuastro-programs-list.html)
to enable easy, robust, and most importantly fast and efficient, data
analysis directly on the command-line. For example it can perform
arithmetic operations on image pixels or table columns/rows, visualize
FITS images as JPG or PDF, convolve an image with a given kernel or
matching of kernels, perform cosmological calculations, crop parts of
large images (possibly in multiple files), manipulate FITS extensions
and keywords, and perform statistical operations. In addition, it
contains programs to make catalogs from detection maps, add noise,
make mock profiles with a variety of radial functions using
monte-carlo integration for their centers, match catalogs, and detect
objects in an image among many other operations. Gnuastro is written
to comply fully with the GNU coding standards and integrates well with
all Unix-like operating systems. This enables astronomers to expect a
fully familiar experience in the building, installing and command-line
user interaction that they have seen in all the other GNU software
that they use (core components in many Unix-like operating
systems). Gnuastro's extensive library is also installed for users who
want to build their own unique/custom programs.

Relevant links:
* Lecture notes: https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/smack-3-gnuastro.md
* Video of this tutorial: https://www.youtube.com/watch?v=6mp5-y5XYSo \
(video corresponds to slightly [older version of this file](https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/688ce6738fe2c6cf9140bed7635a15947e7d542d/smack-3-gnuastro.md); from Commit 688ce6738f)
* Gnuastro's main webpage: https://www.gnu.org/s/gnuastro
* Gnuastro documentation: https://www.gnu.org/s/gnuastro/manual
* Gnuastro tutorials: https://www.gnu.org/s/gnuastro/manual/html_node/Tutorials.html

Working scenario prepared by Mohammad Akhlaghi, Raul Infante-Sainz, Zahra Sharbaf and Joseph Putko.

**Version of Gnuastro:** This SMACK assumes at least **Gnuastro
0.16**. Almost all commands will work on older versions too, but
depending on how old it is, some outputs may differ or programs may
crash (bugs have been fixed).





## Installing Gnuastro with Conda

Due to its [few
dependnecies](https://www.gnu.org/software/gnuastro/manual/html_node/Mandatory-dependencies.html),
Gnuastro can be easily installed from source on macOS or GNU/Linux
operating systems. For a fast trial, you can also use Conda (which
will download pre-built binaries and thus be much faster).

If you already have Conda, you can skip this paragraph and the box
under it and go directly to the commands to setup Gnuastro. If you
don't have conda, you can see the instructions to set it up in a
temporary directory called `conda-install` below (that you can later
delete to completely remove the whole environment from your OS without
affecting your operating system):

```shell
# Download the latest version (GNU/Linux or macOS)
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh

# If you don't have Conda, install it (in a directory named conda-install)
bash Miniconda3-latest-Linux-x86_64.sh -b -p conda-install

# Start Conda in the running terminal.
source conda-install/etc/profile.d/conda.sh
```

Now that Conda is available, you just have to create the Gnuastro
environment and activate it with the first two commands below. You can
then continue with the rest of this tutorial. After you are finished,
you can deactivate it with the last command below.

```shell
# Creating the Gnuastro's conda environment
conda create -n gnuastro -c conda-forge gnuastro

# Activating the Gnuastro environment
conda activate gnuastro

# You can now try all the commands in the next sections...

# After you are finished, Deactivate the Conda environment
conda deactivate
```




### Basic use of Gnuastro programs
By following the next steps, the reader will be able to learn the basic
syntaxis of Gnuastro programs in a practical way.  We will be using images
from the IAC's [ABYSS HUDF WFC3/IR
project](http://research.iac.es/proyecto/abyss), done by Alejandro Borlaff
(ex-PhD student at the IAC). One of its main advantages compared to
previous reductions was the use of
[NoiseChisel](https://www.gnu.org/software/gnuastro/manual/html_node/NoiseChisel.html)
(one of Gnuastro's programs) in the reduction.

1. Start downloading the images with cURL (another command-line
   downloading tool, in [SMACK-2](smack-2-shell.md) we introduced
   Wget, so here, we'll use cURL. But thanks to shell variables and
   loops (reviewed in SMACK-2), this time, we'll download more than
   one image:

   ```shell
   # Put the downloaded images in a 'download' directory:
   mkdir download

   # To download one image we can use this command. In cURL, we
   # should also give the name of the file to download the contents
   # into, we do that with the '-o' option.
   curl -o download/abyss-f125w.fits http://cdsarc.u-strasbg.fr/ftp/J/A+A/621/A133/fits/ah_f125w.fits

   # To download all four filters in one command, we'll use a
   # 'for' loop. Note the use of the 'f' variable that "loops"
   # through the filters and we use it to set the filename.  To
   # make the command easier to read, the URL is also stored as a
   # shell variable.
   url=http://cdsarc.u-strasbg.fr/ftp/J/A+A/621/A133/fits
   for f in f105w f125w f160w; do curl -o download/abyss-$f.fits $url/ah_$f.fits; done
   ```

1. Calling Gnuastro programs: Gnuastro has many programs, but to keep
   them easily identifiable, they all start with `ast`.  For example:
   `astnoisechisel`, `astfits`, `asttable`, ... You can use this to
   easily call your desired program without having to write the full
   name. Try it for your self (press the `TAB` key instead of `<TAB>`:

   ```shell
   ast<TAB><TAB>       # List all of Gnuastro's programs.
   astc<TAB>           # Only select those that start with a 'c'.
   astcr<TAB>          # Only selects 'astcrop'.
   astn<TAB>           # Only selects 'astnoisechisel'
   ```

1. `astfits` is used showing extension and header keywords. This
   program can print or manipulate the FITS file HDUs (extensions),
   meta-data keywords in a given HDU. So, instead of opening the image
   with `DS9`, let's have a look at the data from the command line.

   ```shell
   # View general information about each extension. The ABYSS survey
   # has followed the most robust standard: the first/zero-th
   # extension is kept for metadata ('INFO'), it has no data, but its
   # keywords give information about the whole dataset.
   astfits download/abyss-f125w.fits

   # If you specify a special HDU, with 'astfits', you can see its
   # keywords (image meta-data). Note that HDU counting starts from zero.
   astfits download/abyss-f125w.fits --hdu=0             # These two commands...
   astfits download/abyss-f125w.fits -h0                 # ... are identical!

   # You can also use the actual name of the extension.
   # Above the name of the first extension was 'INFO').
   astfits download/abyss-f125w.fits --hdu=INFO          # These two commands...
   astfits download/abyss-f125w.fits -hINFO              # ... are identical!

   # Now, let's look at its second extension keywords that belong to
   # the image. For example we see that this image was made with
   # Gnuastro, version 0.7. The other group of keywords are the
   # WCS-related keywords.
   astfits download/abyss-f125w.fits -h1

   # Or even better:
   astfits download/abyss-f125w.fits -h1 | awk '/^CDELT1/{print $3}'

   # You can even convert the pixel scale to the more familiar units
   # of arc-seconds, by multiplying it with 3600.
   astfits download/abyss-f125w.fits -h1 | awk '/^CDELT1/{print $3*3600}'

   # Use --pixelscale for obtaining pixel scale information.
   astfits download/abyss-f125w.fits -h1 --pixelscale

   # Use --skycoverage to check the sky coverage of this image.
   astfits download/abyss-f125w.fits -h1 --skycoverage
   ```

1. You can also modify keywords with `astfits`. For example, with the
   first command below, note the absurdly low-values of the `PC1_2`
   and `PC2_1` values: 10^(-17) compared to 1 and -1 for `PC2_2` and
   `PC1_1`. As a demonstration, we'll remove them, such customizations
   are usually necessary when dealing with new software.

   ```shell
   # Note how PC1_2 and PC2_1 are 10^(-17), while PC1_1 is 1!
   astfits download/abyss-f125w.fits -hSCI | grep ^PC

   # So let's delete these two extra headers that may be annoying later.
   astfits download/abyss-f125w.fits -hSCI -hSCI --delete=PC1_2 --delete=PC2_1
   astfits download/abyss-f125w.fits -hSCI | grep ^PC

   # Now we can put it in a loop to fix for all the filters.
   for f in download/abyss*; do astfits $f -hSCI --delete=PC1_2 --delete=PC2_1; done
   ```

1. Look at the images with [SAOImage
   DS9](https://sites.google.com/cfa.harvard.edu/saoimageds9) to have
   some feeling of what the data looks like. SAO ds9 is not installed
   with Gnuastro, it is a separate FITS image viewer. So to check your
   inputs and outputs, it is one of the best options.

   ```shell
   # Or, you can run it on the command-line.
   ds9 download/abyss-f125w.fits

   # You can also set its settings from the command-line.
   ds9 download/abyss-f125w.fits -zscale -zoom to fit
   ```

1. `astrop` for cropping the image in order to select a wanted portion
   of the image.

   ```shell
   # Crop the image around the center (x,y)=(1603,1316), and widths
   # (wx,wy)=(1000,200). This is done in pixels because of the option
   # '--mode=img'
   astcrop download/abyss-f125w.fits -h1 --mode=img --center=1603,1316 --width=500,200

   # Have a look with ds9. Note that the cropped image is created in the
   # current directory, not the directory containing the inputs.
   astfits abyss-f125w_cropped.fits

   # Note that the WCS information of the cropped image is preserved!
   ds9 download/abyss-f125w_cropped.fits

   # We can also select the crop region based on WCS coordinates. For
   # this, use the option '--mode=wcs'. In this case, coordinates and
   # sizes must be in degrees, so a square of width 20 arcseconds, can
   # be written as 20/3600.
   #
   # You can even specify an output name with '--output' or '-o'.
   astcrop download/abyss-f125w.fits --mode=wcs --center=53.15724,-27.78523 --width=20/3600 -ocropped.fits

   # Have a look with ds9 and astfits.
   ds9 cropped.fits
   astfits cropped.fits

   # You can make very complicated crop regions by using DS9 region files.
   # First, define a polygon and save it as polygon.reg, then use Crop
   astcrop download/abyss-f125w.fits --polygon polygon.reg -o polygon.fits

   # Delete the temporary files made here (note that this doesn't
   # delete the '.fits' files in sub-directories.
   rm *.fits
   ```

1. The program `astnoisechisel` detects signal in noise. Presented in
   ADASS 2015 (Sidney). The output of this program is a
   multi-extension image in which each extension correspond to
   different kind of data. Let's use it!

   ```shell
   # Run astnoisechisel with default parameters to obatain a detection map.
   astnoisechisel download/abyss-f125w.fits -onc.fits

   # Have a look at the output with astfits, see the different HDUs
   # 0      NOISECHISEL-CONFIG  Parameters used for astnoisechisel
   # 1      INPUT-NO-SKY        Input data with sky background subtracted
   # 2      DETECTIONS          Detected signal
   # 3      SKY                 Sky background
   # 4      SKY_STD             Standard deviation of the sky background
   astfits nc.fits

   # Open the multi-extension image with DS9 using the option -mecube.
   ds9 -mecube nc.fits
   ```

1. Let's use `astarihmetic` to manipulate the data. It uses the [reverse polish
   notation](https://www.gnu.org/software/gnuastro/manual/html_node/Reverse-polish-notation.html).
   So, we put the operand, and then the operator. It is possible to use simple
   numbers or datasets (images).

   ```shell
   # Multiply 2 by 3, take the square root, run in quiet mode.
   astarithmetic 2 3 x
   astarithmetic 2 3 x sqrt
   astarithmetic 2 3 x sqrt -q

   # Sum two images
   astarithmetic download/abyss-f125w.fits -h1 download/abyss-f160w.fits -h1 + -o sum.fits
   ds9 sum.fits

   # Have a look at the available operators with
   info arithmetic "arithmetic operators"
   ```

   More advanced operations. Using the different `Noisechisel` output
   extensions to mask different parts of an image. To do that, we use the
   operator `where`.

   ```shell
   # Remember the different extensions
   astfits nc.fits

   # Mask all the detections to only see the sky pixels.
   astarithmetic nc.fits -h1 nc.fits -h2 nan where -oonly-sky.fits
   ds9 only-sky.fits

   # Similar command, but explicity saying: mask all pixels equal to 1
   # This is useful if the mask is different of zeros and ones
   astarithmetic nc.fits -h1 nc.fits -h2 1 eq nan where  -oonly-sky.fits

   # Mask the sky regions
   astarithmetic nc.fits -h1 nc.fits -h2 not nan where -oonly-detections.fits

   # Similar command, but explicity saying: mask all pixels equal to 0
   # This is useful if the mask is different of zeros and ones
   astarithmetic nc.fits -h1 nc.fits -h2 0 eq nan where -oonly-detections.fits

   # Inspect the image.
   ds9 only-detections.fits
   ```

1. Segmentation is the process of classifying the signal into higher-level
   constructs.  The program `astsegment` makes the segmentation of the signal
   in different objects.  For example if you have two separate galaxies in one
   image, by default NoiseChisel will give a value of 1 to the pixels of both,
   but after segmentation, the pixels in each will get separate labels.

   ```shell
   # Run astsegment with default parameters using as input data, the output
   # of running astnoisechisel (nc.fits).
   astsegment nc.fits -oseg.fits

   # Have a look at the output with astfits, see the different HDUs
   # 0      SEGMENT-CONFIG  Parameters used for astsegment
   # 1      INPUT           Input data
   # 2      CLUMPS          Detected clumps (non-noise peaks, see image below).
   # 3      OBJECTS         Detected objects (label for all detected pixels).
   # 4      SKY_STD         Standard deviation of the sky background
   astfits seg.fits

   # Open the multi-extension image with DS9 using the option -mecube.
   ds9 -mecube seg.fits

   # Let's go back to 'astarithmetic' again. Now we will use the operator
   # 'set-' to simplify the notation. Use a combination of clumps and
   # objects to mask the input image.
   astarithmetic seg.fits -h1 set-data \
                 seg.fits -h2 set-clumps \
                 seg.fits -h3 set-objects \
                 clumps objects x set-combined \
                 data combined -1206 ne nan where \
                 --output=masked.fits
   ````

1. The program `astmkcatalog` constructs catalogues. MakeCatalog is a
   Gnuastro’s program for localized measurements over a dataset and
   was presented in ADASS 2016 (Trieste). In other words, MakeCatalog
   convert low-level datasets (like images) to high level catalogs.

   ```shell
   # Run MakeCatalog using the result of the previous step in order to obtain
   # a catalogue of objects. The different options are (check with --help):
   # --ids to get the label number.
   # --ra to get the right ascension.
   # --dec to get the declination.
   # --magnitude to get the magnitude.
   # --sn to get the Signal-to-Noise ratio.
   # --zeropoint to set the zero point of the image.
   astmkcatalog seg.fits --ids --ra --dec --magnitude --sn --axisratio --zeropoint=25.94 --clumpscat -ocatalog.fits

   # Have a look at the resulting FITS file.
   astfits catalog.fits
   astfits catalog.fits -h1

   # You can also ask for plain-text catalogs.
   astmkcatalog seg.fits --ids --ra --dec --magnitude --sn --axisratio --zeropoint=25.94 --clumpscat -ocatalog.txt

   # But look at the difference in size with the command below. It is
   # much more efficient in storage and reading/writing to store the
   # catalog as a FITS table. So we'll delete the plain-text tables.
   ls -lh *.txt catalog.fits

   # We'll continue with the FITS table, so let's delete the extra files.
   rm *.txt
   ```
   But with `astfits` we only see the header/meta-data information, not
   the actual data!!! To do that, we have to use the program `asttable`.

1. Gnuastro's `asttable` is a very powerful and versitle tool for
   inspect tables on the command-line. With it, you can read/write,
   select, convert, or show the information of the columns in FITS
   ASCII table, FITS binary table and plain text table files.

   ```shell
   # Use asttable to see all data
   asttable catalog.fits

   # Use asttable with the option -i to see the table meta-data
   asttable catalog.fits -i

   # Show only certain columns, by name
   asttable catalog.fits --column=RA --column=DEC
   asttable catalog.fits -cRA -cDEC
   asttable catalog.fits -cRA,DEC
   asttable catalog.fits -cRA,DEC,MAGNITUDE

   # You can also use the column's number, but names are much better
   # (your code becomes much more easier to read and it won't depend
   # on the position of the column in the table).
   asttable catalog.fits -c2,3,4

   # Select some columns and save them as the output.
   asttable catalog.fits -cOBJ_ID,MAGNITUDE -osub-catalog.fits
   asttable sub-catalog.fits -i

   # You can also only ask for the first two rows, just like piping into the
   # 'head' program, but this is faster (its all in one program!).
   asttable sub-catalog.fits | head 20
   asttable sub-catalog.fits --head=20

   # The option '--sort' can be used for sorting the data. Example:
   # sort by magnitude, and save the output as a plain text file
   asttable catalog.fits --sort=MAGNITUDE -ocatalog-sorted.txt
   asttable catalog-sorted.txt

   # Use the option --range to consider rows with values into a given range (of
   # a particular column). For example, show only rows with magnitudes between
   # 29 and 30 in the clumps catalog.
   asttable catalog.fits -hCLUMPS --range=MAGNITUDE,26.99,27

   # Let's see how many clumps were found:
   asttable catalog.fits -hCLUMPS --range=MAGNITUDE,26,27 | wc -l

   # You can also ask for ranges in more than one column and only
   # select rows that are almost circular (possibly a star!).
   #
   # Note that like the HDU, the case of the column name is irrelevant.
   asttable catalog.fits -hCLUMPS --range=MAGNITUDE,26,27 --range=axis_ratio,0.9,1
   asttable catalog.fits -hCLUMPS --range=MAGNITUDE,26,27 --range=axis_ratio,0.9,1 | wc -l

   # It is also possible to do operations/arithmetic on the columns.
   # Imagine for example you want to obtain the magnitude column in
   # mili-magnitudes, so, you have to multiply that column by 0.001
   asttable catalog.fits -c'arith MAGNITUDE 0.001 x'

   # Show both columns after and before
   asttable catalog.fits -cMAGNITUDE -c'arith $4 0.001 x'

   # There are more useful operators, like wcs-to-im, etc.
   asttable catalog.fits -cRA,DEC -c'arith RA DEC wcs-to-img' --wcsfile download/abyss-f125w.fits
   ```

1. The program `astquery` is super useful for downloading the subset of
   data from databases. Currently available databases are Gaia, NED, and VizieR.

   ```shell
   # To check the available databases within within Gaia or Vizier, filter
   # by author name.
   astquery gaia --information
   astquery gaia --dataset=edr3 -i
   astquery vizier -i --limitinfo="Infante-Sainz"

   # Download a Gaia catalog that overlaps with the image.
   astquery gaia --dataset=edr3 --overlapwith=download/abyss-f125w.fits \
                 -csource_id,ra,dec,phot_bp_mean_mag -ogaia-cat.fits

   # Check the downloaded catalogue
   asttable gaia-cat.fits -i
   ```

1. The program `astmatch` is useful for matching catalogs, you can use
   it to specify which columns from the two catalogs you want in the
   output (merge two input catalogs into one).

   ```shell
   # Let's match our catalog with the Gaia catalog. We need to call both
   # catalogs, specify their HDUs and matching criteria. The match is going
   # to be done over the columns RA,DEC from both catalogs (--col1 and
   # --col2 options). The radius of match is --aperture=1/3600, so in
   # arcsecs.
   astmatch catalog.fits   --hdu=CLUMPS  --ccol1=RA,DEC \
            gaia-cat.fits  --hdu2=1      --ccol2=RA,DEC \
            --aperture=1/3600 -ogaia-matched.fits

   # The output has two extensions, with matching rows
   astfits gaia-matched.fits

   # We can also ask to merge the columns of the matching rows. For
   # example, here we ask for the RA, DEC and magnitdue from the ABYSS
   # catalog, and the magnitude of the matched objects from the Gaia
   # catalog.
   astmatch catalog.fits   --hdu=CLUMPS  --ccol1=ra,dec \
            gaia-cat.fits  --hdu2=1      --ccol2=RA,DEC \
            --aperture=1/3600 --outcols=aRA,aDEC,aMAGNITUDE,bphot_bp_mean_mag -ogaia-matched.fits

   # Now the output only has one extension.
   astfits gaia-matched.fits

   # Let's have a look at how many objects have been considered as match
   asttable gaia-matched.fits -i

   ```

1. The program `aststatistics` is really useful for obtaining
   statistical data from files (images or tables).

   ```shell
   # Let's have a look at the pixel statistics of the NoiseChisel
   # sky-subtracted image (first extension by default).
   aststatistics nc.fits

   # The dynamic range is very high, so let's only look at the
   # distribution of pixels below 0.005
   aststatistics nc.fits --lessthan=0.005

   # Now let's have a look at the statistics of the image without any
   # NoiseChisel detections. Note how compared to 'nc.fits', the mean
   # and median are much more closer to zero.
   aststatistics only-sky.fits

   # You can also ask for a particular value:
   aststatistics nc.fits       --mean --median --std
   aststatistics only-sky.fits --mean --median --std

   # You can use them to estimate the distribution's skewness:
   aststatistics nc.fits       --mean --median --std | awk '{print ($1-$2)/$3}'
   aststatistics only-sky.fits --mean --median --std | awk '{print ($1-$2)/$3}'

   # There are many more statistics:
   aststatistics --help

   # Now, let's look at the statistics on the magnitude column of our catalog.
   aststatistics catalog.fits -cMAGNITUDE

   # Many times, it is necessary to reject outliers value.
   # This is possible using sigma-clipping!!
   aststatistics catalog.fits -cMAGNITUDE --sigmaclip

   # You can also pipe Gnuastro's programs, for example let's see the
   # difference in our magnitudes and the UVUDF magnitudes for the
   # matched objects.
   asttable catalog.fits -c'arith MAGNITUDE 0.001' | aststatistics
   ````

1. As a visual inspection is always a good idea. Let's use `astconvertt` to
   obtain .jpg, .eps, .pdf images from FITS files.

   ```shell
   # Let's make a postage stamp of the cropped image (jpg format).
   astconvertt crop.fits -ocrop.jpg
   eog crop.jpg

   # Because of the pixel values distribution, we can se nothing. Change
   # the maximum value we represent in the output image.
   astconvertt crop.fits --fluxhigh=0.1 -ocrop.jpg
   eog crop.jpg

   # You can also use different color maps, for example SLS or viridis.
   astconvertt crop.fits --fluxhigh=0.1 --colormap=sls -ocrop.jpg
   eog crop.jpg

   astconvertt crop.fits --fluxhigh=0.1 --colormap=viridis -ocrop.jpg
   eog crop.jpg

   # Finally, you can also save the outputs as EPS or PDF to more
   # easily insert into papers.
   astconvertt crop.fits --fluxhigh=0.1 --colormap=sls -ocrop.eps
   evince crop.eps

   astconvertt crop.fits --fluxhigh=0.1 --colormap=sls -ocrop.pdf
   evince crop.pdf
   ```





### Advanced features and analysis
By following the next steps, the reader will learn more advances features.
In what follow, it is explained how to obtain improved color images,
simulate data, make convolution and obtain radial profiles.

1. Until now, `astconvertt` was used for obtaining false-color and
   visualizing the stamps. However, by using different filters it is possible
   to obtain a color image. Consider the f160w f125w and f105w images.

   ```shell
   # The images have to be given from redder to bluer
   astconvertt download/abyss-f160w.fits download/abyss-f125w.fits download/abyss-f105w.fits -g1 -ocolor.jpg
   eog color.jpg

   # It is all black! That is because the huge pixel values range. Let's
   # try clipping the pixel values. The option '-g1' stands for
   # --globalhdu=1, which means that the data is in the extension 1 of all
   # images. Cut also the lines for a better view of the executed line.
   astconvertt download/abyss-f160w.fits \
               download/abyss-f125w.fits \
               download/abyss-f105w.fits -g1 \
               --fluxlow=0.0 --fluxhigh=0.001 -ocolor.jpg
   eog color.jpg

   # Better, but now we are loosing the bright part of the objects. This is
   # because a linear scale is not appropiate to show such a huge range of
   # pixel values typical of astronomical data.
   ```

1. To solve the above problem, we have develop a script that performs an
   asinh transformation in order to change the pixel value distribution
   appropiately so the high and low pixel values can be represented at the
   same time:
   [rgb-asinh.sh](https://gitlab.com/makhlaghi/useful-scripts/-/blob/master/rgb-asinh.sh)
   (see [Lupton et al. 2004](http://doi.org/10.1086/382245)).

   ```shell
   # At the present moment this script is not in Gnuastro. We are working
   # hard to include it as part of Gnuastro as soon as possible. For now,
   # download the script and make it executable.
   wget -O rgb-asinh.sh https://gitlab.com/makhlaghi/useful-scripts/-/raw/master/rgb-asinh.sh?inline=false
   chmod +x rgb-asinh.sh

   # Now we are ready to use this script. It will give you some tips to
   # improve the result. In general, a minimum value to be showed (if the
   # sky background has been put to zero) is --minimum=0.0
   ./rgb-asinh.sh download/abyss-f160w.fits \
                  download/abyss-f125w.fits \
                  download/abyss-f105w.fits \
                  --hdu=1 --minimum=0 -oasinh-color.jpg
   eog asinh-color.jpg

    # Play with the parameters and enhance a bit the contrast with the option --contrast=2.
    ./rgb-asinh.sh download/abyss-f160w.fits \
                   download/abyss-f125w.fits \
                   download/abyss-f105w.fits \
                  --hdu=1 --minimum=0 -oasinh-color.jpg --qbright=0.453879 --contrast=2
   eog asinh-color.jpg

   # This script has many other parameters in order to account for the
   # zeropoint calibration, weighting differently each image for modifying
   # the color and several other features. Look for them!
   ```

1. Astronomical images use to be noisy. A way to mittigate this problem
   is to convolve the image with a kernel in order have a smoother image.
   `astmkprof` is used to generate the kernel, and `astconvolve` for making
   the convolution.

   ```shell
   # First, create the kernel. For example a Moffat with FWHM 3pix,
   # beta=2.8, truncation=5, or a Gaussian with FWHM 3, and truncation=5
   astmkprof --kernel=moffat,3,2.8,5 --oversample=1 -omoffat.fits
   astmkprof --kernel=gaussian,3,5 --oversample=1 -ogaussian.fits

   # Convolve in the spatial domain
   astconvolve download/abyss-f125w.fits --kernel=gaussian.fits --domain=spatial -oconvolved.fits

   # Compare the original and convolved images
   ds9 download/abyss-f125w.fits convolved.fits

   # It is possible to subtract them to study the effect of the
   # convolution. Look at the excess of light in the outer part of the
   # sources.
   astarithmetic convolved.fits -h1 download/abyss-f125w.fits -h1 - -osubtracted.fits
   ds9 subtracted.fits

   # Convolve all the images and obtain a new color images to see how it
   # changes. Compare with the non convolved color image.
   for f in f105w f125w f160w; do \
     astconvolve download/abyss-$f.fits --kernel=moffat.fits --domain=spatial -oabyss-$f-convolved.fits; done

   ./rgb-asinh.sh abyss-f160w-convolved.fits \
                  abyss-f125w-convolved.fits \
                  abyss-f105w-convolved.fits \
                  -H1 -M0 --qbright=0.453879 --contrast=2 -oconvolved.jpg
   eog convolved.jpg asinh-color.jpg
   ```

1. Simulating a stream over a galaxy. By using several programs it is
   possible to make really complicate and realistic things. Step by step.

   ```shell
   # Put the profile properties in a text file called 'cat.txt' (for what each
   # column means, see the link below):
   # https://www.gnu.org/software/gnuastro/manual/html_node/MakeProfiles-catalog.html
   echo "0 0   0   3 5  0 0   1   5    1"  > cat.txt   # PSF
   echo "1 300 300 1 30 1 20  0.8 18   5" >> cat.txt   # Galaxy (Sersic index 1)
   echo "2 300 300 6 80 0 100 0.4 20   1" >> cat.txt   # Stream

   # Build the profile (over-sampled at a scale of 5 times)
   astmkprof cat.txt --mergedsize=600,600 --mode=img --oversample=5 \
                     --zeropoint=30 --circumwidth=20 --output=no-noise-no-conv.fits

   # Have a look
   ds9 no-noise-no-conv.fits

   # Rename the PSF for easy reading.
   mv 0_no-noise-no-conv.fits kernel.fits

   # Convolve the over-sampled profile with the over-sampled PSF
   # (both were created with the 'astmkprof' command above).
   astconvolve no-noise-no-conv.fits --kernel=kernel.fits \
               --output=no-noise-oversampled.fits

   # Undersample the image to the final desired resolution
   astwarp no-noise-oversampled.fits --centeroncorner \
           --scale=1/5 --output=no-noise.fits

   # Add Poisson noise to the image, assuming the sky/background has a magnitude
   # of 26 (the '30' is the same zeropoint as above). See the
   # 'mknoise-poisson' operator in Arithmetic's documentation for more:
   # https://www.gnu.org/software/gnuastro/manual/html_node/Arithmetic-operators.html
   astarithmetic no-noise.fits 26 30 mag-to-counts \
                 mknoise-poisson --output=galaxy-stream.fits
   ```

1. In addition to the Gnuastro programs, we also have Gnuastro scripts.
   They are actual shell scripts with the goal of making high-level
   operations.
   ```shell
   astscript<TAB>                 # List all of Gnuastro's scripts.
   which astscript-radial-profile # Check the path where it is installed.
   vim $(which astscript-radial-profile)
   ```

1. Computing radial profiles. Many times it is necessary to compute the
   averaged radial profiles of astronomical sources. In Gnuastro, there is a
   high-level analysis script specially designed for this task:
   `astscript-radial-profile`.


   ```shell
   # Compute a radial profile with default parameters. It will create a
   # circulary averaged radial profile. Use the previous simulated image.
   # By default, the center will be the centered pixel of the image, and
   # the radius, the maximum possible radius on the image.
   astscript-radial-profile galaxy-stream.fits -ocircular-radial-profile.fits

   # Check the result, plot the result if possible
   asttable circular-radial-profile.fits -i
   asttable circular-radial-profile.fits
   topcat circular-radial-profile.fits

   # It is possible to change the center, both in img or wcs modes.
   # Default is --mode=img (so in pixels). If you want a to center the
   # radial profile at x,y=100,200, and a maximum radius of 14 pixels, then
   # run the following line
   astscript-radial-profile galaxy-stream.fits --center=100,200 --rmax=14 -ocircular-radial-profile.fits

   # Modifying the parameters to make an appropiate radial profile
   # measurement. For example, for the simulated galaxy, the position angle
   # is 30 deg, and the axis ratio is 0.8. In addition to that, it is also
   # possible to make different measures. For example computing mean,
   # median, standard deviation, sigma-clipped mean and sigma-clipped
   # standard deviation:
   astscript-radial-profile galaxy-stream.fits --positionangle=30 --axisratio=0.8 \
                            --measure=mean,median,std,sigclip-mean,sigclip-std -ogalaxy-radial-profile.fits

   # Check and plot the result
   asttable galaxy-radial-profile.fits -i
   topcat galaxy-radial-profile.fits
   ```







### Acknowledgements and citations
Finally, Gnuastro is itself a research project. So, if you use Gnuastro's
program in your research, please cite the necessary papers and add the
necessary acknowledgement statement in your papers. Without formal
recognition/acknowledgment by the community, we cannot get funding for
continued work on it to add new features, fix bugs and etc. To help you
cite the necessary papers, Gnuastro's programs have the option `--cite`,
giving you the BibTeX entries for the paper(s) to cite and necessary
acknowledgement statement. Note that some programs may have special papers
that isn't in the rest, so please try it for all the programs that you have
used.

```shell
# Get the citation biography data
astfits --cite
astmkcatalog --cite
astnoisechisel --cite
```




### Additional material
Playing with Sloan Digital Sky Survey images in order to analyze them and obtain color images.
1. Now, let's look at a ground-based image analysis also. We'll try to
   detect the outer wings of [this extremely metal poor
   ](http://skyserver.sdss.org/dr12/en/tools/chart/navi.aspx?ra=336.8779168&dec=-9.664997118&scale=0.4&width=120&height=120)
   (XMP) galaxy, imaged by the Sloan Digital Sky Survey (SDSS).
   First, let's download the image from the SDSS survey webpage:

   ```shell
   # Let's work in the 'sdss' directory
   mkdir sdss
   cd sdss

   # Download each filter, uncompress it, and give it a short name.
   for f in g r i; do wget http://dr12.sdss3.org/sas/dr12/boss/photoObj/frames/301/2576/2/frame-$f-002576-2-0103.fits.bz2; bzip2 -d *$f-*; mv *$f-* $f.fits; done
   ls

   # Note that the SDSS images have their 2D image in the zero-th extension.
   # The third extension is actually a table.
   astfits g.fits
   ds9 g.fits
   asttable g.fits -h3 -i
   ```

1. The images of each filter are not aligned with the celestial
   coordinates.

   ```shell
   # Let's first test the alignment on the 'g' filter.
   f=g                     # Set the shell variable 'f' to have a value of 'g'.
   astwarp $f.fits -h0 --align --output=$f-aligned.fits

   # Let's have a look! Notice the North and East axises on the top-right
   # they are now aligned with the image and the image has been respectively
   # rotated.
   ds9 $f-aligned.fits -zoom to fit -zscale

   # Let's generalize it for all the filters.
   for f in g r i; do astwarp $f.fits -h0 --align --output=$f-aligned.fits; done
   ls
   ```

1. Also, the images of each filter don't exactly cover the same field
   of view: the XDP is on different pixel positions in each
   filter. You can open them in DS9, and actually see this by
   "Blinking" through the colors

   ```shell
   # Just see the three downloaded images.
   ds9 i.fits r.fits g.fits -tile column

   # Now "Blink" between them to see how the objects in the image are
   # not exactly in the same place in each filter.
   ds9 i.fits r.fits g.fits -single -zscale -match scale -lock frame image -zoom to fit -blink
   ```

1. As a first job on this dataset, let's make an RGB mug-shot JPEG
   image of this galaxy.

   ```shell
   # The RA and Dec of the galaxy is fixed in all images, but its
   # pixel positions differ (as shown above in the blinking). So
   # instead of repeating the values, we'll use variables.
   ra=336.8779168
   dec=-9.664997118

   # First, let's have a look if we imported the values properly.
   echo "$ra $dec"

   # You can use the 'wcstoimg' arithmetic operator of 'asttable' to
   # get the pixel positions of the RA and Dec for each image.
   f=g                     # Set the shell variable 'f' to have a value of 'g'.
   echo "$ra $dec" | asttable --column='arith $1 $2 wcstoimg' --wcsfile=$f-aligned.fits

   # But to use these in 'astcrop', we need a comma (,) between the
   # two pixel coordinate values. So let's use AWK for doing this:
   echo "$ra $dec" | asttable --column='arith $1 $2 wcstoimg' --wcsfile=$f-aligned.fits | awk '{printf "%s,%s\n", $1, $2}'

   # To use the result, we need to put it in a shell variable Note
   # that in effect we are just putting the whole command above
   # (XXXXXX) within this statement 'center=$(XXXXXX)':
   center=$(echo "$ra $dec" | asttable --column='arith $1 $2 wcstoimg' --wcsfile=$f-aligned.fits | awk '{printf "%s,%s", $1, $2}')
   echo $center

   # Now we can crop a box of width 300 pixels, with the galaxy in the
   # center.
   astcrop --mode=img --width=300 --center=$center $f-aligned.fits --output=$f-small.fits
   ds9 g-small.fits

   # Finally, let's generalize the steps above to all three filters
   # with a simple 'for' loop. Note that within the loop we are using
   # exactly the commands above, only the value of 'f' is changed on
   # every cycle of the loop.
   #
   # To make it more generally usable, we can specify the crop width
   # and suffix of the output as the shell variables 'w' and 'suffix'.
   w=300
   suffix=-small.fits
   for f in g r i; do center=$(echo "$ra $dec" | asttable --column='arith $1 $2 wcstoimg' --wcsfile=$f-aligned.fits | awk '{printf "%s,%s", $1, $2}'); astcrop --mode=img --width=$w --center=$center $f-aligned.fits --output=$f$suffix; done

   # Let's see the three centered small crops:
   ds9 i-small.fits r-small.fits g-small.fits -tile column

   # Now let's put them in the Red-Green-Blue channels of a JPEG image.
   astconvertt i-small.fits r-small.fits g-small.fits -h1 -h1 -h1 --output=rgb.jpg

   # Let's have a look at it, we only see the bright star below the
   # image, everything is black.
   eog rgb.jpg

   # We can set the displayed range of pixel values with the
   # '--fluxlow' and '--fluxhigh' options:
   astconvertt i-small.fits r-small.fits g-small.fits -h1 -h1 -h1 --fluxlow=-0.05 --fluxhigh=0.5 --output=rgb.jpg
   ```

1. Now that we have been able to center the galaxy and we want to
   detect the diffuse/extended emission around the galaxy, let's
   co-add the three filters into one deeper image first.

   ```shell
   # We can use exactly the same loop above, just changing the width
   # and suffix
   w=1300
   suffix=-big.fits
   for f in g r i; do center=$(echo "$ra $dec" | asttable --column='arith $1 $2 wcstoimg' --wcsfile=$f-aligned.fits | awk '{printf "%s,%s", $1, $2}'); astcrop --mode=img --width=$w --center=$center $f-aligned.fits --output=$f$suffix; done

   # Let's have a look at the "big" crops.
   ds9 i-big.fits r-big.fits g-big.fits -tile column

   # Now that all three are centered on the galaxy, we can coadd them
   # into one deeper image (by taking the 'mean' of the three)
   astarithmetic i-big.fits r-big.fits g-big.fits 3 mean -g1 --output=deep.fits

   # Let's compare the three inputs and the deeper image:
   ds9 i-big.fits r-big.fits g-big.fits deep.fits -tile column
   ```

1. With the deeper image ready, let's detect the signal within it with
   NoiseChisel and use NoiseChisel's output to generate a
   Signal-to-Noise image.

   ```shell
   # Run NoiseChisel to detect signal.
   astnoisechisel deep.fits -onc.fits

   # Run Arithmetic to generate an S/N image.
   astarithmetic nc.fits -hINPUT-NO-SKY nc.fits -hSKY_STD / --output=sn.fits

   # Compare the sky subtracted image (first NoiseChisel extension)
   # with the S/N image, in particular note the pixel values. Each
   # pixel is now the ratio of signal to noise in that pixel.
   ds9 deep.fits sn.fits

   # Following the division, with arithmetic, we can also only select
   # pixels that have a S/N above 1. Note that we just added a '1 gt'
   # to the previous call and changed the output name.
   astarithmetic nc.fits -hINPUT-NO-SKY nc.fits -hSKY_STD / 1 gt --output=sn-th1.fits
   ```
