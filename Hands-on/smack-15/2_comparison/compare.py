import numpy as np
from sampling import emcee_sequential
from sampling_pycompss import emcee_pycompss


def execution_params():
    """Define execution parameters."""
    np.random.seed(42)
    initial = np.random.randn(128, 8)
    nwalkers, ndim = initial.shape
    nsteps = 10
    return initial, nwalkers, ndim, nsteps


def check_results(result_seq, result_pycompss):
    """Check if all results are equal.

    :param result_seq: Results from sequential execution.
    :param result_pycompss: Results from execution with PyCOMPSs:
    :returns: If the results are equal.
    """
    is_ok = True
    if (
        result_seq.coords.shape != result_pycompss.coords.shape
        or not (result_seq.coords == result_pycompss.coords).all()
    ):
        is_ok = False
    if not (result_seq.log_prob == result_pycompss.log_prob).all():
        is_ok = False
    if result_seq.blobs != result_seq.blobs:
        is_ok = False
    # print(result_seq.random_state)
    # print(result_pycompss.random_state)
    return is_ok


def main():
    """Compare running emcee sequentially and with PyCOMPSs."""
    params = execution_params()

    result_seq = emcee_sequential(params)
    result_pycompss = emcee_pycompss(params)

    is_ok = check_results(result_seq, result_pycompss)
    if is_ok:
        print("OK - Results are the same!")
    else:
        print("FAIL - Results are different!")


if __name__ == "__main__":
    main()
