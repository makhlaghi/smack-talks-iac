######################################################
# Run comparison example job                         #
######################################################
#                                                    #
# Usage sample:                                      #
#                                                    #
#    ./run.sh                                        #
#                                                    #
# CAUTION! Do not run in the LaPalma login node      #
######################################################

pycompss run \
  --log_level=off \
  compare.py
