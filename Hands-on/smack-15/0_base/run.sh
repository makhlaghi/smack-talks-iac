######################################################
# Run sampling example job                           #
######################################################
#                                                    #
# Usage sample:                                      #
#                                                    #
#    ./run.sh                                        #
#                                                    #
# CAUTION! Do not run in the LaPalma login node      #
######################################################

python3 sampling.py
