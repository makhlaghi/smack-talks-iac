from pycompss.api.api import compss_wait_on


def map(func, iterable, chunksize=None):
    result = []
    for i in iterable:
        result.append(func(i))
    result = compss_wait_on(result)
    return result
