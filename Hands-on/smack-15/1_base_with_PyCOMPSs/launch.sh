######################################################
# Submit sampling with PyCOMPSs example job          #
######################################################
#                                                    #
# Usage sample:                                      #
#                                                    #
#    ./launch.sh                                     #
#                                                    #
# Wait for the job to be executed and then you can   #
# check the job out and err files.                   #
######################################################

module load compss/2.10.1

pycompss job submit \
  --qos=debug \
  --log_level=off \
  --job_name=sampling_pycompss \
  --exec_time=5 \
  --num_nodes=2 \
  --reservation=bsc19_113 \
  --graph \
  --tracing \
  sampling_pycompss.py
