SMACK VII: Computing at the IAC
---------------------------------------------------------------

This talk will give an overview of the resources for scientific computing at the IAC. The network, los burros, and LaPalma will be described, together with the basics for using them and practical examples. The session plan is
[available on GitLab](https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/smack-7-iaccomputing.md),
and has also been used in a talk that has been [recorded on YouTube](https://youtu.be/Rn56YemyeJY). The slides are available at ftp://carlos:allende@ftp.ll.iac.es/talks/iaccomputing.pdf.

### Introduction (5 min, Carlos)
1. The IAC network
2. policies
3. ssh and df
4. NAS
5. Supercomputing


### Burros (10 min, Ana Esteban)
1. machines
2. policies
3. htop, top
4. memory, load

### Condor (10 min, Juan Carlos Trelles)
1. Condor
2. examples
3. commands
4. acknowledgement

### diva, deimos and lapalma (10 min, Isaac Alonso)
1. deimos
2. diva
3. lapalma
4. slurm examples

### access to lapalma and deimos/diva (2 min, Carlos)
1. new access policies
2. acknowledgements
3. where to find help
