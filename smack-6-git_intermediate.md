SMACK 6: Intermediate Git
--------------------------------------------------------

### Abstract

Following up on our previous "Git version control system basics" seminar
(https://bit.ly/35CCX7k), the focus of this talk will be to learn the features
of Git that will allow us to collaborate with other colleagues. For this, the
main concepts needed are branch merging (including merges with conflicts),
remote repositories and hosted repositories (GitHub, GitLab, Bitbucket, etc.).

Thanks to the distributed structure of Git, collaboration with colleagues can
become very efficient (allowing for many different workflows) and it avoids the
single point of failure of centralized version control systems, but its
complexity also goes up. Properly understanding the concept of remote
repositories makes collaboration with Git straightforward.

Other Git features and tools that are not essential, but that will make Git
usage much more effective and powerful (like stash, rebase, pull requests, etc.)
will be left for a planned "Advanced Git" follow-up talk.

Relevant links:
* Lecture notes (pdf): https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/Slides/Presentation-git-intermediate.pdf
* Video of this tutorial: https://youtu.be/LBxto3AnQEk


