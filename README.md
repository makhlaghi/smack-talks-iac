Notes for the IAC SMACK series of talks
---------------------------------------

*Short Meetings on Astro Computing Knowledge*(SMACK) are a series of talks, or more appropriately 'live demonstrations', presented in the Instituto de Astrofísica de Canarias ([IAC](http://iac.es))), targeting graduate students and researchers.  The main aim of the talks is to demonstrate the use and benefits of basic software tools that are commonly required for astronomical research. These talks will be showcasted at IAC Talks and recorded for easy future reference by the community.

This repository contains the notes used in each of the talks.
The list of talks is available below.
   - **SMACK 1: Introduction:** history of the POSIX operating systems and basic tools ().
   - **SMACK 2: GNU Bash and basic shell:** Command-line tools and shell scripting.
   - **SMACK 3: GNU Astronomy Utilities (Gnuastro):** Data analysis on the command-line.
   - **SMACK 4: Scripting:** Keeping your commands to re-create exact steps later.
   - **SMACK 5: Git:** Git version control system basics.
   - **SMACK 6: Git:** Intermediate Git.
   - **SMACK 7: Computing at the IAC:** network, deimos/diva, lapalma, condor, RES.
   - **SMACK 8: Git:** Advanced Git.
   - **SMACK 9: The Python Ecosystem for Astronomy:** Python, Numpy & Astropy. High-level programming language popular in astronomy.
   - **SMACK 10: Data Analysis with Python: Pandas Library:** Power library to handle structured data.
   - **SMACK 11: LaTeX:** Programmable tool for building publishable papers and reports.
   - **SMACK 12: Do it faster! Simple ways to use all those cores (and GPUs) efficiently:** Tools for parallel execution of trivially parallel tasks. Command line and pyton (multiprocessing, numba, cupy).
   - **SMACK 13: Containers** (to enclose an environment)
   - **SMACK 14: Make** to organize workflows
   - **SMACK 15: Programming parallel workloads with PyCOMPSs in La Palma:** To parallelize with PyCOMPSs
   - **SMACK 16: LaTeX (beginner)** professional typesetting of your document.



### Notes on plain-text formatting

- The raw plain-text files are written in [Markdown](https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide).
- The files can also be edited directly on the webpage: simply click on the file, and press the "Edit" button in the row of buttons above the file contents.
  In this mode, you can preview the rendering of the file in Markdown, before committing your edits.
  You can also use the "Web IDE" option for a more advanced editor and to edit multiple files.
- **one-sentence-per-line:** This is done to help with recording the changes in Git.





### Further reading

- Utah Univeristy [Hands-on introduction to Linux Series](https://www.chpc.utah.edu/presentations/IntroLinux3parts.php). Just note that they are not introducing the "Linux" kernel (as the name suggests!), but the shell (command-line) environment.
