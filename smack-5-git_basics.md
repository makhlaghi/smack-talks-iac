SMACK 5: Git version control system basics
--------------------------------------------------------

### Abstract

Git is probably the most widely used Version Control System (software tools that
help record changes to files (computer code, text files, etc.) by keeping track
of modifications done through time). It can be used for any type of text files,
though it is specially useful for programming code and it makes managing your
projects, files, and changes made to them much easier and more intuitive.

But it is a big and complex system and people new to it can have a hard time
mastering it. In this talk I will introduce the git version control system to
people that have never used it before, so I will go over its basic concepts and
functionalities from the ground-up. I will cover the most common commands,
needed to use Git for your own individual projects. Once you properly understand
how to use it on your own, it is much easier to understand how to collaborate
with others (for example using GitHub), which will be covered in a follow-up
talk: "Intermediate Git".

Relevant links:
* Lecture notes (pdf): https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/Slides/Presentation-git-basics.pdf
* Video of this tutorial: https://youtu.be/kkNKEJIR-XQ



