SMACK 8: Advanced Git
--------------------------------------------------------

### Abstract

In two previous talks we covered the basics of Git
(http://iactalks.iac.es/talks/view/1426) and some intermediate concepts
(http://iactalks.iac.es/talks/view/1428). In this one we will focus on more
advanced features that can make your Git experience much more productive and
efficient.  First we will focus on a very common collaboration "procedure",
which we didn't have time to cover in any detail in the "Intermediate Git" talk,
namely "Pull requests".  Next, since most of your work with Git will be on local
repositories, we will see some more advanced commands and features, such as pull
requests, rebase, reset, hunks, reflog, stash and blame.  While this series on
Git doesn't cover everything there is to Git, it does cover almost all you'll
need to become a regular Git user.

Relevant links:
* Lecture notes (pdf): https://gitlab.com/makhlaghi/smack-talks-iac/-/blob/master/Slides/Presentation-git-advanced.pdf
* Video of this tutorial: https://youtu.be/-ECItCbzojo


